'use strict';

/**
 * Athlete.js controller
 *
 * @description: A set of functions called "actions" for managing `Athlete`.
 */

module.exports = {

  /**
   * Retrieve athlete records.
   *
   * @return {Object|Array}
   */

  find: async (ctx) => {
    if (ctx.query._q) {
      return strapi.services.athlete.search(ctx.query);
    } else {
      return strapi.services.athlete.fetchAll(ctx.query);
    }
  },

  /**
   * Retrieve a athlete record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    if (!ctx.params._id.match(/^[0-9a-fA-F]{24}$/)) {
      return ctx.notFound();
    }

    return strapi.services.athlete.fetch(ctx.params);
  },

  /**
   * Count athlete records.
   *
   * @return {Number}
   */

  count: async (ctx) => {
    return strapi.services.athlete.count(ctx.query);
  },

  /**
   * Create a/an athlete record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.athlete.add(ctx.request.body);
  },

  /**
   * Update a/an athlete record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.athlete.edit(ctx.params, ctx.request.body) ;
  },

  /**
   * Destroy a/an athlete record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.athlete.remove(ctx.params);
  }
};
