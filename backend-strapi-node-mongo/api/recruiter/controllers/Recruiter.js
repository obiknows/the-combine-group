'use strict';

/**
 * Recruiter.js controller
 *
 * @description: A set of functions called "actions" for managing `Recruiter`.
 */

module.exports = {

  /**
   * Retrieve recruiter records.
   *
   * @return {Object|Array}
   */

  find: async (ctx) => {
    if (ctx.query._q) {
      return strapi.services.recruiter.search(ctx.query);
    } else {
      return strapi.services.recruiter.fetchAll(ctx.query);
    }
  },

  /**
   * Retrieve a recruiter record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    if (!ctx.params._id.match(/^[0-9a-fA-F]{24}$/)) {
      return ctx.notFound();
    }

    return strapi.services.recruiter.fetch(ctx.params);
  },

  /**
   * Count recruiter records.
   *
   * @return {Number}
   */

  count: async (ctx) => {
    return strapi.services.recruiter.count(ctx.query);
  },

  /**
   * Create a/an recruiter record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.recruiter.add(ctx.request.body);
  },

  /**
   * Update a/an recruiter record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.recruiter.edit(ctx.params, ctx.request.body) ;
  },

  /**
   * Destroy a/an recruiter record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.recruiter.remove(ctx.params);
  }
};
