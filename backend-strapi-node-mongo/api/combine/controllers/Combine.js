'use strict';

/**
 * Combine.js controller
 *
 * @description: A set of functions called "actions" for managing `Combine`.
 */

module.exports = {

  /**
   * Retrieve combine records.
   *
   * @return {Object|Array}
   */

  find: async (ctx) => {
    if (ctx.query._q) {
      return strapi.services.combine.search(ctx.query);
    } else {
      return strapi.services.combine.fetchAll(ctx.query);
    }
  },

  /**
   * Retrieve a combine record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    if (!ctx.params._id.match(/^[0-9a-fA-F]{24}$/)) {
      return ctx.notFound();
    }

    return strapi.services.combine.fetch(ctx.params);
  },

  /**
   * Count combine records.
   *
   * @return {Number}
   */

  count: async (ctx) => {
    return strapi.services.combine.count(ctx.query);
  },

  /**
   * Create a/an combine record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.combine.add(ctx.request.body);
  },

  /**
   * Update a/an combine record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.combine.edit(ctx.params, ctx.request.body) ;
  },

  /**
   * Destroy a/an combine record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.combine.remove(ctx.params);
  }
};
