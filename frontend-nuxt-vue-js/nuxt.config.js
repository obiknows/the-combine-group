module.exports = {
  /*
  ** Headers of the page
  */
  head: {
    title: 'The Combine Group',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'TCG Front End UI' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'stylesheet', type: 'text/css', href: '/css/tachyons.css' },
      { rel: 'stylesheet', type: 'text/css', href: '/css/froala_style.min.css' },
      { rel: 'stylesheet', type: 'text/css', href: '/css/froala_blocks.min.css' },
      { rel: 'stylesheet', type: 'text/css', href: '/css/froala_editor.pkgd.min.css' },
    ]
  },
  /*
  ** Additional Modules
  */
  modules: [
    'bootstrap-vue/nuxt',
  ],
  /*
  ** Customize the progress bar color
  */
  loading: { color: '#3B8070' },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** Run ESLint on save
    */
    extend (config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  }
}

