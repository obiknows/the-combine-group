the-combine-group
=================

This was built with this tutorial series: 

https://blog.strapi.io/cooking-a-deliveroo-clone-with-nuxt-vue-js-graphql-strapi-and-stripe-setup-part-1-7/


I used Next JS because it uses React so its easy to pick up by other devs, and allows for alot of routing logic to be automatically handled, and alot of pure HTML/CSS to be used

Languages used: Javascript

Frontend: Next JS

Backend: Strapi.JS


## SITE MAP

**PUBLIC**
```
/            -- home page
/combines    -- combines 
/huddle      -- articles
/features    -- features page
/pricing     -- pricing page

/athletes    -- atheletes pros page
/recruiters  -- recruiters pros pages

"Accounts"
/signup      -- sign up page
/login       -- log in page
/logout      -- log out page         (TODO - create)
/forgot-pass -- forgot password page (TODO - create)
/reset-pass  -- reset password page  (TODO - create)

"Profile"
/{username}  -- public profile for athlete or recruiter ((TODO - finish design))

```

**AUTHENTICATED**
```
/onboard     -- initial onboarding page
  /athelete  -- onboard athelete (fill out athlete user type for user)
  /recruiter -- onbooard recruiter (fill out recruiter user type for user)

/dashboard   -- dashboard for athletes & recruiters
  /home      -- snippets from daily huddle, notifications, messaging request
  /chat      -- messages (based on user_id)
  /huddle    -- customized daily huddle and news
  /settings  -- see profile status, change payments, etc.

 "Athletes"
  /profile   -- input stats, change pictures, uploads videos, photos, personal [edit profile]

 "Recruiters"
  /scout     -- search for athletes (by location, height/weight, age, position, etc.) 
  /recruit   -- set desired athlete params and auto scout/ notify
  /profile   -- show recruit conversion rate. recruit prospecting chart
```


## APP FLOW

1. First Time (User, Athlete)
```
--> (/) --> (/signup) --> (/onboard) --> (/onboard/athlete) --> (/tour) --> (/dashboard)
```
2. First Time (User, Recruiter)
```
--> (/) --> (/signup) --> (/onboard) --> (/onboard/recruiter) --> (/tour) --> (/dashboard)
```
3. Returning User (Athlete)
```
--->(/) --> (/profile) --> (/:feature)
--->(/) --> (/dashboard) --> (/huddle -- read the daily huddle)
```

## TODO

#### Authentication 
[links to codesamples](https://strapi.io/documentation/3.x.x/guides/authentication.html#registration)


- [ ] Finish Home Page `(/)`
- [ ] Finish Combine Page `(/combine)`
  - [ ] Get All Combines (GET /combines)
- [ ] Finish Huddle Page `(/huddle)`
  - [ ] Get All Articles (GET /articles)
  - [ ] Create Articles (POST /articles)
- [x] Test sign up/login
- [x] Have working log in
- [x] Test login, commit
- [x] Have working log out
- [x] Test log out, commit
- [ ] Scaffold out profiles (/:profile) 
  - [ ] --> if (role == 'athlete')   --> { layout: athlete-dashboard }
  - [ ] --> if (role == 'recruiter') --> { layout: recruiter-dashboard }
- [ ] Test & View Profiles

- [x] Deploy Frontend + Backend
- [ ] Test all User Flows

### Post Prototype

- [ ] Add in Forgot Password/Reset Password
- [ ] Test Forgot Password on Live Domain

#### Inspirations

- [Athlete Profile](https://dribbble.com/shots/4547919-Athlete-Profile/attachments/1029062)

- [Athelete Profile, Responsive](https://dribbble.com/shots/5592521-Sport-transfers-Player-profile-Mobile/attachments)
